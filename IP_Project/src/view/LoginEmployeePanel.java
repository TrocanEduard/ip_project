/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.sql.Connection;
import java.sql.SQLException;

import control.Connect;
import functions.Functions;

/**
 *
 * @author trocky
 */
public class LoginEmployeePanel extends javax.swing.JPanel {

    /**
     * Creates new form LoginEmployeeJPanel
     * @throws SQLException 
     */
    public LoginEmployeePanel() throws SQLException {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     * @throws SQLException 
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() throws SQLException {

        passwordField = new javax.swing.JPasswordField();
        password = new javax.swing.JLabel();
        userName = new javax.swing.JLabel();
        usernameTextField = new javax.swing.JTextField();
        forgotaccountButton = new javax.swing.JButton();
        loginButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        
        myconn = new Connect();
		conn = myconn.CreateConnection();

        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Login Employee", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(0, 0, 255))); // NOI18N

        password.setText("Password");

        userName.setText("Username");

        forgotaccountButton.setText("Forgot account?");
   
        forgotaccountButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
					forgotaccountButtonActionPerformed(evt);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });

        loginButton.setText("Login");
       
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
					loginButtonActionPerformed(evt);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(backButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(userName)
                            .addComponent(password))
                        .addGap(56, 56, 56)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(loginButton)
                                .addGap(18, 18, 18)
                                .addComponent(forgotaccountButton))
                            .addComponent(passwordField)
                            .addComponent(usernameTextField))))
                .addContainerGap(99, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backButton)
                .addGap(59, 59, 59)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userName)
                    .addComponent(usernameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(password)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loginButton)
                    .addComponent(forgotaccountButton))
                .addContainerGap(109, Short.MAX_VALUE))
        );
    }// </editor-fold>                        

                            

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {                                           
    	
    	MainFrame.welcomeFrame.kingPanel.removeAll();
    	MainFrame.welcomeFrame.kingPanel.repaint();
    	MainFrame.welcomeFrame.kingPanel.setLayout(new java.awt.BorderLayout());
    	MainFrame.welcomeFrame.kingPanel.add(MainFrame.welcomeFrame.firstPanel);
    	MainFrame.welcomeFrame.kingPanel.setVisible(true);
    	this.setVisible(true);
    }                                          

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) throws SQLException {                                            
    	String user = usernameTextField.getText();
		String pass = passwordField.getText();
		Functions f = new Functions();
		String hash = f.sha256(pass);
		System.out.println(hash);

		if(user.length() > 0 && pass.length() > 0){
			if(myconn.Login(conn, user, hash))
			{
				usernameTextField.setText("");
				passwordField.setText("");
				//this.dispose();
			}


		}	       
    }                                           

    private void forgotaccountButtonActionPerformed(java.awt.event.ActionEvent evt) throws SQLException {                                                    
    	ForgotEmployeePanel panel =new ForgotEmployeePanel();
    	MainFrame.welcomeFrame.kingPanel.removeAll();
    	MainFrame.welcomeFrame.kingPanel.repaint();
    	MainFrame.welcomeFrame.kingPanel.setLayout(new java.awt.BorderLayout());
    	MainFrame.welcomeFrame.kingPanel.add(panel);
    	MainFrame.welcomeFrame.kingPanel.validate();
    	MainFrame.welcomeFrame.kingPanel.repaint();
    	panel.setVisible(true);
    	MainFrame.welcomeFrame.kingPanel.setVisible(true);
    	this.setVisible(true);
    }                                                   


    // Variables declaration - do not modify                     
    private javax.swing.JButton backButton;
    private javax.swing.JButton forgotaccountButton;
    private javax.swing.JButton loginButton;
    private javax.swing.JLabel password;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel userName;
    private javax.swing.JTextField usernameTextField;
    // End of variables declaration   
	Connection conn ;
	Connect myconn;
}
