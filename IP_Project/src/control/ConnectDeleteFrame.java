package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDeleteFrame {

	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}
	
	public void DeleteBank(String name,Connection conn) throws SQLException
	{
		String querry = "DELETE FROM Banca"
					   + " WHERE nume_Banca = "
					   + "'" + name + "'" ;
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
	
	public void DeleteBranch(String name, Connection conn) throws SQLException
	{
		String querry = "DELETE FROM Sucursala"
					   + " WHERE nume_Sucursala = "
					   + "'" + name + "'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
	
	public void DeleteClient(String SSN, String name, Connection conn) throws SQLException
	{
		String querry = " DELETE FROM persoana_fizica"
				      + " where CNP_Persoana_Fizica = " + SSN
					  +	" And nume_Clientt = " + name
					  + ";" ;
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
	
	public void DeleteCompany(String SSN, String name, Connection conn) throws SQLException
	{
		String querry = " DELETE FROM Companie"
				      + " where CUI_Companie = " + SSN
					  +	" And nume_Companie= " + name
					  + ";" ;
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
	

}
