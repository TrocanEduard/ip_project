package control;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import email.Email;
import model.Angajat;
import view.AdminFrame;
import view.MainFrame;
import view.MainFrameAdmin;
import view.WelcomeFrame;


public class Connect 
{
	
	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}
	
	
	
	public boolean Login (Connection conn , String user, String password) throws SQLException
	{
		
		    String querry = "Select * From Angajat where userName_Angajat="
		    				+ "'" +user +"'" 
		    				+" and password_Angajat="
		    				+"'" +password+ "'";
        	Statement stmt = conn.createStatement();
        	ResultSet rs = stmt.executeQuery(querry);
            
            if(rs.next()){
                MainFrame.welcomeFrame.dispose();
                
                try {
                    for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                } catch (ClassNotFoundException ex) {
                    java.util.logging.Logger.getLogger(AdminFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    java.util.logging.Logger.getLogger(AdminFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    java.util.logging.Logger.getLogger(AdminFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                    java.util.logging.Logger.getLogger(AdminFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                //</editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        MainFrameAdmin.Run();
                    }
                });
                
                conn.close();
                return true;
     
            }
            else{
                JOptionPane.showMessageDialog(null,"Invalid User or Password");
                return false;
            }
	}
	
	public void ForgotAccount (Connection conn , String name, String SSN) throws SQLException{
		  
			String querry = "Select * From Angajat where nume_Angajat="
					+ "'" + name +"'" 
					+" and CNP_Angajat="
					+"'" +SSN+ "'";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(querry);
			
			if(rs.next()){
                JOptionPane.showMessageDialog(null,"Email was sent");
                Angajat a = new Angajat();
                String username = rs.getString("userName_Angajat");
                String password = rs.getString("password_Angajat");
                 
                
                Email e =  new Email();
        		e.sendEmail("UserName: "+username+"\nPassword: "+ password ,"trocan.eduard@gmail.com");
        		conn.close();
			}
			else{
				JOptionPane.showMessageDialog(null,"Invalid user or Password");
			}
	}
	
	public void UpdatePassword (Connection conn , String codeFromMail,String yourUsername, String newPassword) throws SQLException{
		  
		String querry = "Select * From Angajat where password_Angajat= "
				+ "'" + codeFromMail +"'" 
				+" and userName_Angajat = "
				+"'" +yourUsername+ "'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		if(rs.next()){
			
			String querry2 = "Update Angajat"
							+ " Set password_Angajat = "
							+ "'" +newPassword+"'"
							+ " Where userName_Angajat = " 
							+ "'" +yourUsername+"'"
							+";";
			Statement stmt2 = conn.createStatement();
			stmt2.executeUpdate(querry2);
             
    		conn.close();
    		JOptionPane.showMessageDialog(null,"The password has been changed!");
    		MainFrame.welcomeFrame.kingPanel.removeAll();
        	MainFrame.welcomeFrame.kingPanel.repaint();
        	MainFrame.welcomeFrame.kingPanel.setLayout(new java.awt.BorderLayout());
        	MainFrame.welcomeFrame.kingPanel.add(MainFrame.welcomeFrame.firstPanel);
        	MainFrame.welcomeFrame.kingPanel.setVisible(true);
        	
		}
		else{
			JOptionPane.showMessageDialog(null,"Invalid code or user");
		}
	}
}
