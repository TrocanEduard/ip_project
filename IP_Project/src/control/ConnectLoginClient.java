package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import email.Email;
import model.Angajat;

import view.ClientFrame;
import view.MainFrame;
import view.MainFrameClient;

public class ConnectLoginClient {

	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}
	
	
	
	public boolean LoginClient (Connection conn , String user, String password) throws SQLException
	{
		
		    String querry =   "Select * From Persoana_Fizica , Companie"
		    	            + " where ( userName_Clientt ="
		    				+ "'" +user +"'" 
		    				+" OR"
		    				+" userName_Companie = "
		    				+ "'" +user +"'" 
		    				+")"
		    				+" and ( password_Clientt="
		    				+"'" +password+ "'"
		    				+" OR"
		    				+" password_Companie="
		    				+"'" +password+ "'"
		    				+");";
		    System.out.println(querry);
        	Statement stmt = conn.createStatement();
        	ResultSet rs = stmt.executeQuery(querry);
            
            if(rs.next()){
                MainFrame.welcomeFrame.dispose();

                try {
                    for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                } catch (ClassNotFoundException ex) {
                    java.util.logging.Logger.getLogger(ClientFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    java.util.logging.Logger.getLogger(ClientFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    java.util.logging.Logger.getLogger(ClientFrame.class.getName()).log(java.util.logging.Level.SEVERE ,null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                    java.util.logging.Logger.getLogger(ClientFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
             

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                         MainFrameClient.Run();
                    }
                });
                
                conn.close();
                return true;
     
            }
            else{
                JOptionPane.showMessageDialog(null,"Invalid User or Password");
                return false;
            }
	}
	
	public void ForgotAccountClient (Connection conn , String name, String SSN) throws SQLException{
		  
			String querry = "Select * From Persoana_Fizica, Companie "
					+ "where (nume_Clientt="
					+ "'" + name +"'" 
					+ " OR "
					+ "nume_Companie = "
					+ "'" + name +"'" 
					+")"
					+" and (CNP_Persoana_Fizica="
					+"'" +SSN+ "'"
					+ " OR "
					+" CUI_Companie = "
					+"'" +SSN+ "'"
					+");" ;
			System.out.println(querry);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(querry);
			
			if(rs.next()){
               String username_PersoanaFizica = "";
               String password_PersoanaFizica = "";
               String username_Companie = "";
               String password_Companie = "";
               
                 username_PersoanaFizica= rs.getString("userName_Clientt");
                 password_PersoanaFizica = rs.getString("password_Clientt");
                 username_Companie= rs.getString("userName_Companie");
                 password_Companie= rs.getString("password_Companie");
                 
                
                Email e =  new Email();
                if(username_PersoanaFizica.length()>0 && password_PersoanaFizica.length() > 0)
                {
                	e.sendEmail("UserName: "+username_PersoanaFizica+"\nPassword: "+ password_PersoanaFizica ,"trocan.eduard@gmail.com");
                }
                else
                {
                	e.sendEmail("UserName: "+username_Companie+"\nPassword: "+ password_Companie ,"trocan.eduard@gmail.com");
                }
        		JOptionPane.showMessageDialog(null,"Email was sent");
        		conn.close();
			}
			else{
				JOptionPane.showMessageDialog(null,"Invalid data");
			}
	}
	
	public void UpdatePasswordClient (Connection conn , String codeFromMail,String yourUsername, String newPassword) throws SQLException{
		  
		String querry = "Select * From Persoana_Fizica "
				+ "where password_Clientt= "
				+ "'" + codeFromMail +"'" 
				+" and userName_Clientt = "
				+"'" +yourUsername+ "'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		if(rs.next()){
			
			String querry2 = "Update Persoana_Fizica"
							+ " Set password_Clientt = "
							+ "'" +newPassword+"'"
							+ " Where userName_Clientt = " 
							+ "'" +yourUsername+"'"
							+";";
			Statement stmt2 = conn.createStatement();
			stmt2.executeUpdate(querry2);
    		conn.close();
    		JOptionPane.showMessageDialog(null,"The password has been changed!");
    		MainFrame.welcomeFrame.kingPanel.removeAll();
        	MainFrame.welcomeFrame.kingPanel.repaint();
        	MainFrame.welcomeFrame.kingPanel.setLayout(new java.awt.BorderLayout());
        	MainFrame.welcomeFrame.kingPanel.add(MainFrame.welcomeFrame.firstPanel);
        	MainFrame.welcomeFrame.kingPanel.setVisible(true);
    		
		}
		else{
			String querry3 = "Select * From Companie"
					+ "where password_Companie= "
					+ "'" + codeFromMail +"'" 
					+" and userName_Companie = "
					+"'" +yourUsername+ "'";
			System.out.println(querry3);
			Statement stmt3 = conn.createStatement();
			ResultSet rs3 = stmt3.executeQuery(querry3);
			
			if(rs3.next()){
				
				String querry4 = "Update Companie"
								+ " Set password_Companie = "
								+ "'" +newPassword+"'"
								+ " Where userName_Companie = " 
								+ "'" +yourUsername+"'"
								+";";
				System.out.println(querry4);
				Statement stmt4 = conn.createStatement();
				stmt4.executeUpdate(querry4);
	             
	    		conn.close();
	    		JOptionPane.showMessageDialog(null,"The password has been changed!");
	    		MainFrame.welcomeFrame.kingPanel.removeAll();
	        	MainFrame.welcomeFrame.kingPanel.repaint();
	        	MainFrame.welcomeFrame.kingPanel.setLayout(new java.awt.BorderLayout());
	        	MainFrame.welcomeFrame.kingPanel.add(MainFrame.welcomeFrame.firstPanel);
	        	MainFrame.welcomeFrame.kingPanel.setVisible(true);
			}
			else
			{			
				JOptionPane.showMessageDialog(null,"Invalid code or user");
			}
		}
	}
}
