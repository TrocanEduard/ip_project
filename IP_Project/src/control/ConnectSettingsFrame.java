package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import view.AdminFrame;
import view.LoginEmployeePanel;
import view.MainFrame;

public class ConnectSettingsFrame {

	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}
	
	public boolean UpdatePasswordEmployee(Connection conn,String username,String oldPassword ,String newPassword) throws SQLException
	{
		String querry = "Select * From Angajat where password_Angajat= "
				+ "'" + oldPassword +"'" 
				+" and userName_Angajat = "
				+"'" +username+ "'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		if(rs.next()){
			
			String querry1 =  " UPDATE Angajat"
					   + " SET password_Angajat = "
					   + "'" + newPassword + "'"
					   + " WHERE userName_Angajat ="
					   + "'" + username + "'"
					   + "and password_Angajat ="
					   + "'" + oldPassword + "'"
					   +";";
			Statement stmt1 = conn.createStatement();
			stmt1.executeUpdate(querry1);
		
			JOptionPane.showMessageDialog(null,"The password has been changed!\nPlease login again");
			return true;
		}
		else{
			JOptionPane.showMessageDialog(null,"Invalid Username or Password");
			return false;
		}
	}
	
	public boolean UpdateUsernameEmployee(Connection conn,String username,String password ,String newUsername) throws SQLException
	{
		String querry = "Select * From Angajat where password_Angajat= "
				+ "'" + password +"'" 
				+" and userName_Angajat = "
				+"'" +username+ "'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		if(rs.next()){
			
			String querry1 =  " UPDATE Angajat"
					   + " SET userName_Angajat = "
					   + "'" + newUsername + "'"
					   + " WHERE userName_Angajat ="
					   + "'" + username + "'"
					   + "and password_Angajat ="
					   + "'" + password + "'"
					   +";";
			Statement stmt1 = conn.createStatement();
			stmt1.executeUpdate(querry1);
		
			JOptionPane.showMessageDialog(null,"The password has been changed!\nPlease login again");
			return true;
    	
		}
		else{
			JOptionPane.showMessageDialog(null,"Invalid Username or Password");
			return false;
		}
	}
}
