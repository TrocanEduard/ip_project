package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ConnectViewFrame {
	


	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}

	public ArrayList<String> AfisareBanci (Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<String>();
		Object[] rowData = new Object[3];
		String querry = "SELECT * FROM Banca";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		while(rs.next()){
	    
			list.add(rs.getString("cod_Banca"));
			list.add(rs.getString("nume_Banca"));
			list.add(rs.getString("adresa_Banca"));
			
		}
		return list;
	}
	
	public ArrayList<String> ViewAllBanksFromCity (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT * FROM Banca  "
					   +"Where adresa_Banca = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		while(rs.next()){
			list.add(rs.getString("cod_Banca"));
			list.add(rs.getString("nume_Banca"));
			list.add(rs.getString("adresa_Banca"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllBranches (Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<String>();
		String querry = "SELECT * FROM Sucursala";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		while(rs.next()){
			list.add(rs.getString("nume_Sucursala"));
			list.add(rs.getString("adresa_Sucursala"));
			list.add(rs.getString("cod_Banca"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllBranchesFromCity (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT * FROM Sucursala  "
					   +"Where adresa_Sucursala = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		while(rs.next()){
			list.add(rs.getString("nume_Sucursala"));
			list.add(rs.getString("adresa_Sucursala"));
			list.add(rs.getString("cod_Banca"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllBranchesOfTheBank (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT nume_Sucursala, adresa_Sucursala, nume_Banca "
						+" FROM Sucursala, Banca "
					    +" Where Banca.cod_Banca = Sucursala.cod_Banca and "
					    + " nume_Banca LIKE " 
					    + " '%"+item+"%'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		while(rs.next()){
			list.add(rs.getString("nume_Sucursala"));
			list.add(rs.getString("adresa_Sucursala"));
			list.add(rs.getString("nume_Banca"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllEmployees (Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<String>();
		String querry = "SELECT CNP_Angajat, nume_Angajat, adresa_Angajat,nume_Sucursala "
					  + "FROM Angajat";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		while(rs.next()){
			list.add(rs.getString("CNP_angajat"));
			list.add(rs.getString("nume_Angajat"));
			list.add(rs.getString("adresa_Angajat"));
			list.add(rs.getString("nume_Sucursala"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllEmployeesFromCity (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT  CNP_Angajat, nume_Angajat, adresa_Angajat,nume_Sucursala "
					  + " FROM Angajat"
					  + " Where adresa_Angajat = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);

		while(rs.next()){
			list.add(rs.getString("CNP_angajat"));
			list.add(rs.getString("nume_Angajat"));
			list.add(rs.getString("adresa_Angajat"));
			list.add(rs.getString("nume_Sucursala"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllEmployeesOfTheBranch (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT  CNP_Angajat, nume_Angajat, adresa_Angajat,nume_Sucursala "
					  + " FROM Angajat"
					  + " Where nume_Sucursala = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);

		while(rs.next()){
			list.add(rs.getString("CNP_angajat"));
			list.add(rs.getString("nume_Angajat"));
			list.add(rs.getString("adresa_Angajat"));
			list.add(rs.getString("nume_Sucursala"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllClientsThatHaveADeposit (Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<String>();
		String querry = " Select Depune.nume_Sucursala, Persoana_Fizica.nume_Clientt, Persoana_Fizica.adresa_Clientt"
					  + " From Depune , Persoana_Fizica"
					  + " Where Depune.cod_Clientt = Persoana_fizica.cod_Clientt" ;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		while(rs.next()){
	    
			list.add(rs.getString("nume_Sucursala"));
			list.add(rs.getString("nume_Clientt"));
			list.add(rs.getString("adresa_Clientt"));
			
		}
		return list;
	}
	
	public ArrayList<String> ViewAllClientsThatHaveADepositFromCity (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT Depune.nume_Sucursala, Persoana_Fizica.nume_Clientt, Persoana_Fizica.adresa_Clientt"
				      + " From Depune , Persoana_Fizica"
			       	  + " Where Depune.cod_Clientt = Persoana_fizica.cod_Clientt and Persoana_Fizica.adresa_Clientt = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);

		while(rs.next()){
			list.add(rs.getString("Depune.nume_Sucursala"));
			list.add(rs.getString("Persoana_Fizica.nume_Clientt"));
			list.add(rs.getString("Persoana_Fizica.adresa_Clientt"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllClientsThatHaveADepositOfTheBranch (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT Depune.nume_Sucursala, Persoana_Fizica.nume_Clientt, Persoana_Fizica.adresa_Clientt"
				      + " From Depune , Persoana_Fizica"
			       	  + " Where Depune.cod_Clientt = Persoana_fizica.cod_Clientt and Depune.nume_Sucursala = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);

		while(rs.next()){
			list.add(rs.getString("Depune.nume_Sucursala"));
			list.add(rs.getString("Persoana_Fizica.nume_Clientt"));
			list.add(rs.getString("Persoana_Fizica.adresa_Clientt"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllClientsThatHaveALoan (Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<String>();
		String querry = " Select Imprumuta.nume_Sucursala, Persoana_Fizica.nume_Clientt, Persoana_Fizica.adresa_Clientt"
					  + " From Imprumuta , Persoana_Fizica"
					  + " Where Imprumuta.cod_Clientt = Persoana_fizica.cod_Clientt" ;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		while(rs.next()){
	    
			list.add(rs.getString("nume_Sucursala"));
			list.add(rs.getString("nume_Clientt"));
			list.add(rs.getString("adresa_Clientt"));
			
		}
		return list;
	}
	
	public ArrayList<String> ViewAllClientsThatHaveALoanFromCity (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT Imprumuta.nume_Sucursala, Persoana_Fizica.nume_Clientt, Persoana_Fizica.adresa_Clientt"
				      + " From Imprumuta , Persoana_Fizica"
			       	  + " Where Imprumuta.cod_Clientt = Persoana_fizica.cod_Clientt and Persoana_Fizica.adresa_Clientt = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);

		while(rs.next()){
			list.add(rs.getString("Imprumuta.nume_Sucursala"));
			list.add(rs.getString("Persoana_Fizica.nume_Clientt"));
			list.add(rs.getString("Persoana_Fizica.adresa_Clientt"));
		}
		return list;
	}
	
	public ArrayList<String> ViewAllClientsThatHaveALoanOfTheBranch (String item , Connection conn) throws SQLException
	{
		ArrayList<String> list = new ArrayList<>();
		String querry = " SELECT Imprumuta.nume_Sucursala, Persoana_Fizica.nume_Clientt, Persoana_Fizica.adresa_Clientt"
				      + " From Imprumuta , Persoana_Fizica"
			       	  + " Where Imprumuta.cod_Clientt = Persoana_fizica.cod_Clientt and Imprumuta.nume_Sucursala = "+ item;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);

		while(rs.next()){
			list.add(rs.getString("Imprumuta.nume_Sucursala"));
			list.add(rs.getString("Persoana_Fizica.nume_Clientt"));
			list.add(rs.getString("Persoana_Fizica.adresa_Clientt"));
		}
		return list;
	}
	
}
