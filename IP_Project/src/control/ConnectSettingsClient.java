package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import view.AdminFrame;
import view.LoginClientPanel;
import view.LoginEmployeePanel;
import view.MainFrame;

public class ConnectSettingsClient {

	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}
	
	public boolean UpdatePasswordClient(Connection conn,String username,String oldPassword ,String newPassword) throws SQLException
	{
		String querry = "Select * From Persoana_Fizica "
				+ "where password_Clientt= "
				+ "'" + oldPassword +"'" 
				+" and userName_Clientt = "
				+"'" +username+ "'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		if(rs.next()){
			
			String querry1 =  " UPDATE Persoana_Fizica"
					   + " SET password_Clientt = "
					   + "'" + newPassword + "'"
					   + " WHERE userName_Clientt ="
					   + "'" + username + "'"
					   + "and password_Clientt ="
					   + "'" + oldPassword + "'"
					   +";";
			Statement stmt1 = conn.createStatement();
			stmt1.executeUpdate(querry1);
		
			JOptionPane.showMessageDialog(null,"The password has been changed!\nPlease login again");
			return true;
    	
		}
		else{
			
			String querry2 = "Select * From Companie "
					+ "where password_Companie= "
					+ "'" + oldPassword +"'" 
					+" and userName_Companie = "
					+"'" +username+ "'";
			Statement stmt2 = conn.createStatement();
			ResultSet rs2 = stmt2.executeQuery(querry2);
			
			if(rs2.next()){
				
				String querry3 =  " UPDATE Companie"
						   + " SET password_Companie= "
						   + "'" + newPassword + "'"
						   + " WHERE userName_Companie ="
						   + "'" + username + "'"
						   + "and password_Companie ="
						   + "'" + oldPassword + "'"
						   +";";
				Statement stmt3 = conn.createStatement();
				stmt3.executeUpdate(querry3);
			
				JOptionPane.showMessageDialog(null,"The password has been changed!\nPlease login again");
				return true;
	    	
			}
			else{
				JOptionPane.showMessageDialog(null,"Invalid Username or Password");
				return false;
			}
		}
	}
	
	public boolean UpdateUsernameClient(Connection conn,String username,String password ,String newUsername) throws SQLException
	{
		String querry = "Select * From Persoana_Fizica "
				+ "where password_Clientt= "
				+ "'" + password +"'" 
				+" and userName_Clientt = "
				+"'" +username+ "'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		if(rs.next()){
			
			String querry1 =  " UPDATE Persoana_Fizica"
					   + " SET userName_Clientt = "
					   + "'" + newUsername + "'"
					   + " WHERE userName_Clientt ="
					   + "'" + username + "'"
					   + "and password_Clientt ="
					   + "'" + password + "'"
					   +";";
			Statement stmt1 = conn.createStatement();
			stmt1.executeUpdate(querry1);
		
			JOptionPane.showMessageDialog(null,"The password has been changed!\nPlease login again");
			return true;
    	
		}
		else{
			
			String querry2 = "Select * From Companie "
					+ "where password_Companie= "
					+ "'" + password +"'" 
					+" and userName_Companie = "
					+"'" +username+ "'";
			Statement stmt2 = conn.createStatement();
			ResultSet rs2 = stmt2.executeQuery(querry2);
			
			if(rs2.next()){
				
				String querry3 =  " UPDATE Companie"
						   + " SET userName_Companie= "
						   + "'" + newUsername + "'"
						   + " WHERE userName_Companie ="
						   + "'" + username + "'"
						   + "and password_Companie ="
						   + "'" + password + "'"
						   +";";
				Statement stmt3 = conn.createStatement();
				stmt3.executeUpdate(querry3);
			
				JOptionPane.showMessageDialog(null,"The username has been changed!\nPlease login again");
				return true;
	    	
			}
			else{
				JOptionPane.showMessageDialog(null,"Invalid Username or Password");
				return false;
			}
		}
	}
}