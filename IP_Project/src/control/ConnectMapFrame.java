package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;

import map.RedPoint;

public class ConnectMapFrame {
	
	private static WaypointPainter waypointPainter = new WaypointPainter();
	private static JXMapKit map = new JXMapKit();
	
	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}
	
	public static LinkedList<RedPoint> redPoints(Connection conn) throws SQLException{
		
		LinkedList<RedPoint> puncte = new LinkedList<RedPoint>();
		double latitudine;
		double longitudine;
		
		String querry = "SELECT * FROM Banca";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		while(rs.next()){
			latitudine = rs.getDouble("latitudine_Banca");
			longitudine = rs.getDouble("longitudine_Banca");
			
			Waypoint w = new Waypoint(latitudine,longitudine);
			waypointPainter.getWaypoints().add(w);
			System.out.println("latitudine: " + latitudine + " Longitudine: " + longitudine);
			map.getMainMap().setOverlayPainter(waypointPainter);
			/*RedPoint pct = new RedPoint(latitudine,longitudine);
			puncte.add(pct);*/
		}
		return puncte;
	}
	
public static LinkedList<RedPoint> bluePoints(Connection conn) throws SQLException{
		
		LinkedList<RedPoint> puncte = new LinkedList<RedPoint>();
		int latitudine;
		int longitudine;

		String querry = "SELECT * FROM Sucursala";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);
		
		while(rs.next()){
			latitudine = rs.getInt("latitudine_Sucursala");
			longitudine = rs.getInt("longitudine_Sucursala");
			RedPoint pct = new RedPoint(latitudine,longitudine);
			puncte.add(pct);
		}
		return puncte;
	}
}
