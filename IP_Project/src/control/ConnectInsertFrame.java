package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectInsertFrame {

	public Connection CreateConnection () throws SQLException
	{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/ip_project_5", "root", "ildoctore$!^49");	
		return conn;
	}
	
	public void InsertBank(String bankName,String bankAdress, Double lat,Double lon, Connection conn) throws SQLException
	{
		String querry = " INSERT INTO Banca (nume_Banca,adresa_Banca,latitudine_Banca, longitudine_Banca)"
					   + " VALUES ("
					   + "'" + bankName + "'"
					   + ","
					   + "'"+ bankAdress + "'"
					   + ","
					   +  lat 
					   + ","
					   +  lon 
					   + ");";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
	
	public void InsertBranch(String branchName ,String branchAdress,Integer bankId, Connection conn) throws SQLException
	{
		String querry = " INSERT INTO Sucursala (nume_Sucursala , adresa_Sucursala, cod_Banca)"
					   + " VALUES ("
					   + "'" + branchName +"'"
					   + ","
					   + "'" + branchAdress + "'"
					   + ","
					   + "'"+ bankId + "'"
					   + ");";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
	
	public void InsertClient(String clientSSN ,String clientName,String clientAdress,String clientUsername,String clientPassword,Integer clientId, Connection conn) throws SQLException
	{
		String querry = " INSERT INTO Persoana_Fizica (CNP_Persoana_Fizica,nume_Clientt,adresa_Clientt,username_Clientt,password_Clientt,cod_Clientt)"
					   + " VALUES ("
					   + "'" + clientSSN +"'"
					   + ","
					   + "'" + clientName + "'"
					   + ","
					   + "'"+ clientAdress + "'"
					   + ","
					   + "'" + clientUsername +"'"
					   + ","
					   + "'" + clientPassword + "'"
					   + ","
					   + "'"+ clientId + "'"
					   + ");";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
	
	public void InsertCompany(String companyCUI ,String companyName,String companyAdress,String companyUsername,String companyPassword,Integer companyId, Connection conn) throws SQLException
	{
		String querry = " INSERT INTO Companie (CUI_Companie,nume_Companie,adresa_Companie,username_Companie,password_Companie,cod_Clientt)"
					   + " VALUES ("
					   + "'" + companyCUI +"'"
					   + ","
					   + "'" + companyName + "'"
					   + ","
					   + "'"+ companyAdress + "'"
					   + ","
					   + "'" + companyUsername +"'"
					   + ","
					   + "'" + companyPassword + "'"
					   + ","
					   + "'"+ companyId + "'"
					   + ");";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(querry);
	}
}
