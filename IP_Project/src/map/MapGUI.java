package map;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;

import control.Connect;
import control.ConnectInsertFrame;
import control.ConnectMapFrame;
import view.InsertPanel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class MapGUI extends javax.swing.JPanel implements MouseListener {

	private JTextField longitude = new JTextField();
	private JTextField latitude = new JTextField();
	private JTextField bankName = new JTextField();
	private JTextField bankAdress = new JTextField();
	private JButton insertButton = new JButton("Insert");
	
	private int ovalRadius = 0;
	private Double lastZoom;
	private Point lastClick = null;
	private GeoPosition lastClickedLocation;
	private WaypointPainter waypointPainter = new WaypointPainter();
	private JXMapKit map;
	private Waypoint lastWayPoint;
//	private Suprafata suprafata;
	private int myNumber = 88;
	
	
	 

	public MapGUI() throws SQLException {
		super();

		this.setVisible(true);
		this.setBounds(150, 150, 750, 500);
		this.setLayout(null);

		map = (JXMapKit) (new SetupMap()).createOpenMap();
		map.setBounds(20, 20, 690, 400);
		map.getMainMap().addMouseListener(this);
		add(map);

		JLabel lblLong = new JLabel("Longitude");
		JLabel lblLat = new JLabel("Latitude");
		JLabel lblBankName = new JLabel("Bank Name");
		JLabel lblBankAdress = new JLabel("Bank Adress");
		
		
		lblLong.setBounds(263, 420, 150, 25);
		lblLat.setBounds(420, 420, 150, 25);
		lblBankName.setBounds(20, 420, 150, 25);
		lblBankAdress.setBounds(140, 420, 150, 25);
		
		add(lblLong);
		add(lblLat);
		add(lblBankAdress);
		add(lblBankName);
		
		longitude.setBounds(263, 450, 135, 25);
		latitude.setBounds(420, 450, 135, 25);
		bankName.setBounds(20, 450, 100, 25);
		bankAdress.setBounds(140, 450, 100, 25);
		
		insertButton.setBounds(560, 448, 70, 25);
		
		add(longitude);
		add(latitude);
		add(bankAdress);
		add(bankName);
		add(insertButton);
		
		 insertButton.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                try {
						insertButtonActionPerformed(evt);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        });
		
		insertButton.setVisible(true);
		generateRandomPoints();
		
		AddAllWaypoints();
		repaint();

		// Add a sample waypoint.

		AddAllWaypointsBranch();
		repaint();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {

		JXMapViewer m = map.getMainMap();
		GeoPosition g = m.convertPointToGeoPosition(e.getPoint());

		System.out.println(" geo coordinates:  " + g);
		latitude.setText("" + g.getLatitude());
		longitude.setText("" + g.getLongitude());

		if (e.getButton() == MouseEvent.BUTTON1) {

			lastClick = e.getPoint();
			lastClickedLocation = g;
			ovalRadius = 0;
			moveWaypoint(g);

		} else {
			if (lastClick != null) {
				ovalRadius = ((Double) (lastClick.distance(e.getPoint())))
						.intValue();
				waypointPainter.getWaypoints().remove(lastWayPoint);
				latitude.setText("");
				longitude.setText("");
				System.out.println("lastClickedlocation:"+lastClickedLocation);
				System.out.println("center:" + g);
				lastZoom=lastClick.distance(map.getMainMap().getCenter());
			} else
				moveWaypoint(g);

		}
		repaint();
	}

	
	
	public void moveWaypoint(GeoPosition g) {
		Waypoint w = new Waypoint(g.getLatitude(), g.getLongitude());
			
			//Modify this if you need to add a set of markers instead of a single one.
			if (waypointPainter.getWaypoints().isEmpty() || waypointPainter.getWaypoints().size() <= 5) {
				waypointPainter.getWaypoints().add(w);
			}
			else {
				if (waypointPainter.getWaypoints().size() >= 5) {
					waypointPainter.getWaypoints().add(w);
					waypointPainter.getWaypoints().remove(lastWayPoint);
				}
			}
			lastWayPoint = w;
			map.getMainMap().setOverlayPainter(waypointPainter);

			
		}
	
	public void AddAllWaypoints() throws SQLException {	
		
		double latitudine;
		double longitudine;
		myconn = new ConnectMapFrame (); 
		conn = myconn.CreateConnection();
		String querry = "SELECT * FROM Banca";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);		
		while(rs.next()){
			latitudine = rs.getDouble("latitudine_Banca");
			longitudine = rs.getDouble("longitudine_Banca");
			
			Waypoint w = new Waypoint(latitudine,longitudine);
			waypointPainter.getWaypoints().add(w);
		}
		
		waypointPainter.setRenderer(new WaypointRenderer() {
			@Override
			public boolean paintWaypoint(Graphics2D g, JXMapViewer map,
					Waypoint wp) {
				float alpha = 0.6f;
				int type = AlphaComposite.SRC_OVER;
				AlphaComposite composite = AlphaComposite.getInstance(type,
						alpha);
				Color color = new Color(1, 0, 0, alpha); // Red

				g.setColor(color);
				g.fillOval(-5, -5, 10,10);
				g.setColor(Color.BLACK);
				g.drawOval(-5, -5, 10,10); 
				
				
				return true;
			}

		});
		
		repaint();
	}
	
	public void AddAllWaypointsBranch() throws SQLException {
		
		double latitudine;
		double longitudine;
		myconn = new ConnectMapFrame (); 
		conn = myconn.CreateConnection();
		String querry = "SELECT * FROM Sucursala";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(querry);		
		while(rs.next()){
			latitudine = rs.getDouble("latitudine_Sucursala");
			longitudine = rs.getDouble("longitudine_Sucursala");
			
			Waypoint w = new Waypoint(latitudine,longitudine);
			waypointPainter.getWaypoints().add(w);

		}
		waypointPainter.setRenderer(new WaypointRenderer() {
		@Override
		public boolean paintWaypoint(Graphics2D g, JXMapViewer map,
				Waypoint wp) {
			float alpha = 0.6f;
			int type = AlphaComposite.SRC_OVER;
			AlphaComposite composite = AlphaComposite.getInstance(type,
					alpha);
			Color color = new Color(0, 0, 1, alpha); // Blue

			g.setColor(color);
			g.fillOval(-5, -5, 10,10);
			g.setColor(Color.BLACK);
			g.drawOval(-5, -5, 10,10);
			return true;
		}

	});
	
		
		repaint();
	}
	

	public void generateRandomPoints(){
		JXMapViewer m = map.getMainMap();
		for(int i = 0; i < 1; i++){
			int latitudine = 108;
			int longitudine = -75;
			GeoPosition g = m.convertPointToGeoPosition(new Point(latitudine,longitudine));
			moveWaypoint(g);
		}
		repaint();
	}

	
	  private void insertButtonActionPerformed(java.awt.event.ActionEvent evt) throws SQLException {                                             
		     myconn2 = new ConnectInsertFrame();
		     conn2= myconn.CreateConnection();
		     list = new ArrayList<String>(); 
		     
		  if(bankName.getText().isEmpty()==true || bankAdress.getText().isEmpty()==true || latitude.getText().isEmpty()==true || longitude.getText().isEmpty()==true)
		  {
			  
			  JOptionPane.showMessageDialog(null,"Something is wrong! Please insert again!");
	         	bankName.setText("");
	        	bankAdress.setText("");
	        	longitude.setText("");
	        	latitude.setText("");
		  }
		  else
		  {
			    String name = bankName.getText();
		        String adress = bankAdress.getText();  
		        String lat1 = latitude.getText();
		        Double lat = Double.parseDouble(lat1);
		        String lon1 = longitude.getText();
		        Double lon = Double.parseDouble(lon1); 
	        
		        if(name.length() > 0 && adress.length() > 0 )
		        {
		        	myconn2.InsertBank(name, adress,lat,lon, conn);
		        	bankName.setText("");
		        	bankAdress.setText("");
		        	longitude.setText("");
		        	latitude.setText("");
		        }
		  }
		 }
	    

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	 private ConnectMapFrame myconn ;
	 private Connection conn;
	 
	 private Connection conn2 ;
	    private ConnectInsertFrame myconn2;
	    private ArrayList<String> list;
}
