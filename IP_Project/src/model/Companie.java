package model;

public class Companie {
	
	String CUI_Companie;
	String numeC;
	String adresaC;
	String usernameC;
	String passwordC;
	Integer codClient;
	public Companie(String cUI_Companie, String numeC, String adresaC, String usernameC, String passwordC,
			Integer codClient) {
		super();
		CUI_Companie = cUI_Companie;
		this.numeC = numeC;
		this.adresaC = adresaC;
		this.usernameC = usernameC;
		this.passwordC = passwordC;
		this.codClient = codClient;
	}
	public String getCUI_Companie() {
		return CUI_Companie;
	}
	public void setCUI_Companie(String cUI_Companie) {
		CUI_Companie = cUI_Companie;
	}
	public String getNumeC() {
		return numeC;
	}
	public void setNumeC(String numeC) {
		this.numeC = numeC;
	}
	public String getAdresaC() {
		return adresaC;
	}
	public void setAdresaC(String adresaC) {
		this.adresaC = adresaC;
	}
	public String getUsernameC() {
		return usernameC;
	}
	public void setUsernameC(String usernameC) {
		this.usernameC = usernameC;
	}
	public String getPasswordC() {
		return passwordC;
	}
	public void setPasswordC(String passwordC) {
		this.passwordC = passwordC;
	}
	public Integer getCodClient() {
		return codClient;
	}
	public void setCodClient(Integer codClient) {
		this.codClient = codClient;
	}
	
	
}
