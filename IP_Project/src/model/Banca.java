package model;

public class Banca {

	private Integer cod_Banca;
	private String nume_Banca;
	private String adresa_Banca;
	
	public Banca(Integer cod_Banca, String nume_Banca, String adresa_banca) {
		super();
		this.cod_Banca = cod_Banca;
		this.nume_Banca = nume_Banca;
		this.adresa_Banca = adresa_banca;
	}
	
	public Integer getCod_Banca() {
		return cod_Banca;
	}

	public void setCod_Banca(Integer cod_Banca) {
		this.cod_Banca = cod_Banca;
	}

	public String getNume_Banca() {
		return nume_Banca;
	}

	public void setNume_Banca(String nume_Banca) {
		this.nume_Banca = nume_Banca;
	}

	public String getAdresa_banca() {
		return adresa_Banca;
	}

	public void setAdresa_banca(String adresa_banca) {
		this.adresa_Banca = adresa_banca;
	}

	
}
