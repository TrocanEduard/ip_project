package model;

public class Angajat {

	private String CNP_Angajat;
	private String nume_Angajat;
	private String adresa_Angajat;
	private String userName_Angajat;
	private String passward_Angajat;
	private String nume_Sucursala;
	
	public Angajat(String CNP_Angajat, String nume_Angajat, String adresa_Angajat, String userName_Angajat,
			String passward_Angajat, String nume_Sucursala) {
		super();
		this.CNP_Angajat = CNP_Angajat;
		this.nume_Angajat = nume_Angajat;
		this.adresa_Angajat = adresa_Angajat;
		this.userName_Angajat = userName_Angajat;
		this.passward_Angajat = passward_Angajat;
		this.nume_Sucursala = nume_Sucursala;
	}

	public Angajat(){
		CNP_Angajat="";
		nume_Angajat="";
		adresa_Angajat="";
		userName_Angajat="";
		passward_Angajat="";
		nume_Sucursala="";
		
	}

	public String getCNP_Angajat() {
		return CNP_Angajat;
	}

	public void setCNP_Angajat(String cNP_Angajat) {
		CNP_Angajat = cNP_Angajat;
	}

	public String getNume_Angajat() {
		return nume_Angajat;
	}

	public void setNume_Angajat(String nume_Angajat) {
		this.nume_Angajat = nume_Angajat;
	}

	public String getAdresa_Angajat() {
		return adresa_Angajat;
	}

	public void setAdresa_Angajat(String adresa_Angajat) {
		this.adresa_Angajat = adresa_Angajat;
	}

	public String getUserName_Angajat() {
		return userName_Angajat;
	}

	public void setUserName_Angajat(String userName_Angajat) {
		this.userName_Angajat = userName_Angajat;
	}

	public String getPassward_Angajat() {
		return passward_Angajat;
	}

	public void setPassward_Angajat(String passward_Angajat) {
		this.passward_Angajat = passward_Angajat;
	}

	public String getNume_Sucursala() {
		return nume_Sucursala;
	}

	public void setNume_Sucursala(String nume_Sucursala) {
		this.nume_Sucursala = nume_Sucursala;
	}
	
	
}
