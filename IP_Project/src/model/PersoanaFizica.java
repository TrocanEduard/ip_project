package model;

public class PersoanaFizica {

	String CNP_PersoanaFizica;
	String numePF;
	String adresaPF;
	String usernamePF;
	String passwordPF;
	Integer codClient;
	
	public PersoanaFizica(String cNP_PersoanaFizica, String numePF, String adresaPF, String usernamePF,
			String passwordPF, Integer codClient) {
		super();
		CNP_PersoanaFizica = cNP_PersoanaFizica;
		this.numePF = numePF;
		this.adresaPF = adresaPF;
		this.usernamePF = usernamePF;
		this.passwordPF = passwordPF;
		this.codClient = codClient;
	}

	public String getCNP_PersoanaFizica() {
		return CNP_PersoanaFizica;
	}

	public void setCNP_PersoanaFizica(String cNP_PersoanaFizica) {
		CNP_PersoanaFizica = cNP_PersoanaFizica;
	}

	public String getNumePF() {
		return numePF;
	}

	public void setNumePF(String numePF) {
		this.numePF = numePF;
	}

	public String getAdresaPF() {
		return adresaPF;
	}

	public void setAdresaPF(String adresaPF) {
		this.adresaPF = adresaPF;
	}

	public String getUsernamePF() {
		return usernamePF;
	}

	public void setUsernamePF(String usernamePF) {
		this.usernamePF = usernamePF;
	}

	public String getPasswordPF() {
		return passwordPF;
	}

	public void setPasswordPF(String passwordPF) {
		this.passwordPF = passwordPF;
	}

	public Integer getCodClient() {
		return codClient;
	}

	public void setCodClient(Integer codClient) {
		this.codClient = codClient;
	}
	
}
