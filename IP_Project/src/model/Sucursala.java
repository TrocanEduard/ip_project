package model;

public class Sucursala {

	private String nume_Sucursala;
	private String adresa_Sucursala;
	private Integer cod_Banca;
	public Sucursala(String nume_Sucursala, String adresa_Sucursala, Integer cod_Banca) {
		super();
		this.nume_Sucursala = nume_Sucursala;
		this.adresa_Sucursala = adresa_Sucursala;
		this.cod_Banca = cod_Banca;
	}
	
	public String getNume_Sucursala() {
		return nume_Sucursala;
	}
	public void setNume_Sucursala(String nume_Sucursala) {
		this.nume_Sucursala = nume_Sucursala;
	}
	public String getAdresa_Sucursala() {
		return adresa_Sucursala;
	}
	public void setAdresa_Sucursala(String adresa_Sucursala) {
		this.adresa_Sucursala = adresa_Sucursala;
	}
	public Integer getCod_Banca() {
		return cod_Banca;
	}
	public void setCod_Banca(Integer cod_Banca) {
		this.cod_Banca = cod_Banca;
	}
	
}
