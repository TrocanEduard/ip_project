package functions;

import java.security.MessageDigest;
import java.util.concurrent.Callable;

public class Functions implements Callable<String> {
	
	String myName="";
	String hash="";
	Integer intervalStart;
	Integer intervalEnd;
	
	//TODO: check off all the todos
	
	/**
	 * TODO: this constructor should receive an interval where to generate passwords 
	 * and the password hash 
	 * TODO: all three parameters must be saved locally just like name
	 * 
	 * @param intervalStart int
	 * @param intervalEnd int
	 * @param hash String
	 * @param name name of the PasswordTester instance
	 */
	public void PasswordTester(String name,Integer intervalStart,Integer intervalEnd){
		myName=name;//save name locally in this class
		this.intervalEnd = intervalEnd;
		this.intervalStart = intervalStart;
		//Aici modific hashul!!!
		hash="B3920D87448D7695ADE27C7549F9D64CEF66272A6B046D0F1204BE0CA455E38C";
		
		// at this point , name dies!!
	}
	
	
	
	@Override
	public String call() throws Exception {
		Integer i = 0;
		String ret = "Nothing found";
		System.out.println(myName+Thread.currentThread().getId()+": i'm starting");
		
		//TODO: all passwords from intervalStart to intervalEnd must be hashed using sha256 
		//TODO: and checked if their sha256 hash is equal to  the locally saved hash from the constructor
		
		for(i=intervalStart;i<intervalEnd;i++){
			//System.out.println(i+":"+sha256(i));
			if(sha256(i).equals(hash)){
				System.out.println("Found password:"+i);
				return i.toString();
			}
		}
		return ""; 
	}
	
 
	public String sha256(String possiblePassword) {
		 try{
		        MessageDigest digest = MessageDigest.getInstance("SHA-256");
		        byte[] hash = digest.digest((""+possiblePassword).getBytes("UTF-8"));
		        StringBuffer hexString = new StringBuffer();

		        for (int i = 0; i < hash.length; i++) {
		            String hex = Integer.toHexString(0xff & hash[i]);
		            if(hex.length() == 1) hexString.append('0');
		            hexString.append(hex);
		        }

		        return hexString.toString().toUpperCase();
		    } catch(Exception ex){
		       throw new RuntimeException(ex);
		    }
	}
}
