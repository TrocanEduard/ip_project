import java.util.Random;
import java.util.concurrent.locks.Lock;


public class Philosopher implements Runnable {
	private int id;
	private Lock leftChopstick;
	private Lock rightChopstick;
	private Random timeGenerator = new Random();
	public Philosopher(){

	}
	public Philosopher(int id,Lock leftChopstick, Lock rightChopstick){
		this.id=id;
		this.leftChopstick=leftChopstick;
		this.rightChopstick=rightChopstick;
	}
	public void run(){
		try {
			while (true) {
				think(); // astepata
				pickUpChopstick(leftChopstick,rightChopstick); //incearca sa ridice betele
				eat(); // incearca sa manance 
				putDownChopsticks(); // in pune jos
			}
		} catch (InterruptedException e) {
			System.out.println("Philosopher " + id + " was interrupted.\n");   
		}

	}

	private void think() throws InterruptedException {
		System.out.println("Philosopher " + id + " is thinking.\n");
		System.out.flush();

		Thread.sleep (1000+timeGenerator.nextInt(1000));
	}
	private void pickUpChopstick(Lock leftChopstick,Lock rightChopstick){

		while(true){
			boolean lc=false;
			boolean rc=false;
			try{
				lc=leftChopstick.tryLock();//inceaca sa obtina betigasul si daca da il face pe lc true si faca lock pe leftcho
				rc=rightChopstick.tryLock();
			}finally{
				if(lc&&rc){
					System.out.println("Philosopher " + id + " Pick Up both Chop Stick.\n");
					//quote[id]=3;
					return;
				}
				if(lc){
					System.out.println("Philosopher " + id + " Pick Up  leftChop Stick And RightChop Stick is Busy.\n");
					leftChopstick.unlock();
					System.out.println("Philosopher " + id + " Put Down  leftChop Stick.\n");
					//quote[id]=1;
				}
				if(rc){
					System.out.println("Philosopher " + id + " Pick Up  RightChop Stick And lefttChop Stick is Busy.\n");
					rightChopstick.unlock();
					System.out.println("Philosopher " + id + " Put Down  RightChop Stick.\n");
					//quote[id]=2;
				}
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	private void eat() throws InterruptedException {
		Thread.sleep(50);
		System.out.println("Philosopher " + id + " is eating.\n");

		System.out.flush();//Sterge Streamul
		Thread.sleep (200+timeGenerator.nextInt(200));


	}
	private void putDownChopsticks() {
		leftChopstick.unlock();
		rightChopstick.unlock();
		System.out.println("Philosopher " + id + " put down Both ChopStick\n");

	}

}

