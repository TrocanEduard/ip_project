public class Main{
	Chopstick chopstick[];
	Philosopher philosophers[];
	public Main(){
		initializePos();
		initializePhilosopher();
		new Thread(philosophers[0]).start();
		new Thread(philosophers[1]).start();
		new Thread(philosophers[2]).start();
		new Thread(philosophers[3]).start();
		new Thread(philosophers[4]).start();
	}
	public void initializePos(){
		chopstick=new Chopstick[5];
		chopstick[0]=new Chopstick(0);
		chopstick[1]=new Chopstick(1);
		chopstick[2]=new Chopstick(2);
		chopstick[3]=new Chopstick(3);
		chopstick[4]=new Chopstick(4);
	}
	public void initializePhilosopher(){
		philosophers = new Philosopher[5];
		philosophers[0]=new Philosopher(0,chopstick[0],chopstick[4]);
		philosophers[1]=new Philosopher(1,chopstick[1],chopstick[0]);
		philosophers[2]=new Philosopher(2,chopstick[2],chopstick[1]);
		philosophers[3]=new Philosopher(3,chopstick[3],chopstick[2]);
		philosophers[4]=new Philosopher(4,chopstick[4],chopstick[3]);
	}
	public static void main(String[] args){
		new Main();
	}
}